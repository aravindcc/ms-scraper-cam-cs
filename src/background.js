"use strict";

// With background scripts you can communicate with popup
// and contentScript files.
// For more information on background script,
// See https://developer.chrome.com/extensions/background_pages

const opts = [
  ["Collate all courses ms", "get_courses_ms"],
  ["Collate all courses questions", "get_courses_qs"],
  ["Collate all ms", "get_ms"],
  ["Collate all qs", "get_qs"],
  ["Run Blind SQL", "run_blind_sql"],
];

opts.forEach((e) => {
  chrome.contextMenus.create({
    title: e[0],
    contexts: ["all"],
    onclick: (info, tab) =>
      chrome.tabs.sendMessage(tab.id, { text: e[1] }, console.log),
  });
});
